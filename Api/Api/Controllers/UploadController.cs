﻿using Api.Models.Entrada;
using Api.Models.Saida;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web.Http;

namespace Api.Controllers
{
    public class UploadController : ApiController
    {
        // Pegando o caminho salvo dentro do Web.Config com a Key CaminhoTemp
        public static string CaminhoTemporario { set; get; } = ConfigurationSettings.AppSettings["CaminhoTemp"].ToString();

        [HttpPost]
        public Zipado Upload(Parametros parametros)
        {
            #region PROPRIEDADES
            // Todas propriedades para a solução de upload
            var NomeArquivo = $"Arquivo{ DateTime.Now.ToString().Replace(" ", "").Replace(":", "").Replace("-", "").Replace("/", "")}";
            var Diretorio = $@"{CaminhoTemporario + NomeArquivo}";
            var ArquivoZip = $@"{Diretorio}.zip";
            var ListArq = new List<Arquivos>();
            #endregion

            // Pegando o valor em Base64 e convertendo para Byte
            byte[] file = Convert.FromBase64String(parametros.Base64Arquivo);

            // Salvando arquivo em pasta
            File.WriteAllBytes(ArquivoZip, file.ToArray());

            // Extraindo arquivo depois de salvo
            ZipFile.ExtractToDirectory(ArquivoZip, Diretorio);

            // Lendo diretorio do arquivo após extrair o arquivo
            DirectoryInfo diretorio = new DirectoryInfo(Diretorio);

            // Lendo todos os arquivos da pasta extraida
            FileInfo[] ArquivosPastas = diretorio.GetFiles("*");

            // Percorrendo todos os arquivos e inserindo na lista para exibir o nome do arquivo em tela
            foreach (FileInfo item in ArquivosPastas)
            {
                var Arq = new Arquivos() { 
                     NomeArquivo = item.Name,
                     TamanhoArquivo = item.Length
                };
                ListArq.Add(Arq);
            }

            // Terminando de colocar os dados na classe Zipado para mostrar em tela os dados para o usuário
            var Arquivo = new Zipado()
            {
                NomeArquivo = ArquivoZip,
                Arquivos = ListArq
            };

            return Arquivo;
        }
    }
}
