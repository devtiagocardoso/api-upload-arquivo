﻿namespace Api.Models.Saida
{
    public class Arquivos
    {
        public string NomeArquivo { set; get; }

        public long TamanhoArquivo { set; get; }
    }
}