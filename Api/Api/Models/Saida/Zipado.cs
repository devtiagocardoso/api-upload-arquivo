﻿using System.Collections.Generic;

namespace Api.Models.Saida
{
    public class Zipado
    {
        public string NomeArquivo { set; get; }

        public List<Arquivos> Arquivos { set; get; }
    }
}